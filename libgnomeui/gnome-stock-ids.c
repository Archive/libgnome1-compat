/*
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
  @NOTATION@
 */

#include "gnome-stock-ids.h"

const char gnome_stock_pixmap_new[]="New";
const char gnome_stock_pixmap_open[]="Open";
const char gnome_stock_pixmap_close[]="Close";
const char gnome_stock_pixmap_revert[]="Revert";
const char gnome_stock_pixmap_save[]="Save";
const char gnome_stock_pixmap_save_as[]="Save As";
const char gnome_stock_pixmap_cut[]="Cut";
const char gnome_stock_pixmap_copy[]="Copy";
const char gnome_stock_pixmap_paste[]="Paste";
const char gnome_stock_pixmap_clear[]="Clear";
const char gnome_stock_pixmap_properties[]="Properties";
const char gnome_stock_pixmap_preferences[]="Preferences";
const char gnome_stock_pixmap_help[]="Help";
const char gnome_stock_pixmap_scores[]="Scores";
const char gnome_stock_pixmap_print[]="Print";
const char gnome_stock_pixmap_search[]="Search";
const char gnome_stock_pixmap_srchrpl[]="Search/Replace";
const char gnome_stock_pixmap_back[]="Back";
const char gnome_stock_pixmap_forward[]="Forward";
const char gnome_stock_pixmap_first[]="First";
const char gnome_stock_pixmap_last[]="Last";
const char gnome_stock_pixmap_home[]="Home";
const char gnome_stock_pixmap_stop[]="Stop";
const char gnome_stock_pixmap_refresh[]="Refresh";
const char gnome_stock_pixmap_undo[]="Undo";
const char gnome_stock_pixmap_redo[]="Redo";
const char gnome_stock_pixmap_timer[]="Timer";
const char gnome_stock_pixmap_timer_stop[]="Timer Stop";
const char gnome_stock_pixmap_mail[]="Mail";
const char gnome_stock_pixmap_mail_rcv[]="Receive Mail";
const char gnome_stock_pixmap_mail_snd[]="Send Mail";
const char gnome_stock_pixmap_mail_rpl[]="Reply to Mail";
const char gnome_stock_pixmap_mail_fwd[]="Forward Mail";
const char gnome_stock_pixmap_mail_new[]="New Mail";
const char gnome_stock_pixmap_trash[]="Trash";
const char gnome_stock_pixmap_trash_full[]="Trash Full";
const char gnome_stock_pixmap_undelete[]="Undelete";
const char gnome_stock_pixmap_spellcheck[]="Spellchecker";
const char gnome_stock_pixmap_mic[]="Microphone";
const char gnome_stock_pixmap_line_in[]="Line In";
const char gnome_stock_pixmap_cdrom[]="Cdrom";
const char gnome_stock_pixmap_volume[]="Volume";
const char gnome_stock_pixmap_midi[]="Midi";
const char gnome_stock_pixmap_book_red[]="Book Red";
const char gnome_stock_pixmap_book_green[]="Book Green";
const char gnome_stock_pixmap_book_blue[]="Book Blue";
const char gnome_stock_pixmap_book_yellow[]="Book Yellow";
const char gnome_stock_pixmap_book_open[]="Book Open";
const char gnome_stock_pixmap_about[]="About";
const char gnome_stock_pixmap_quit[]="Quit";
const char gnome_stock_pixmap_multiple[]="Multiple";
const char gnome_stock_pixmap_not[]="Not";
const char gnome_stock_pixmap_convert[]="Convert";
const char gnome_stock_pixmap_jump_to[]="Jump To";
const char gnome_stock_pixmap_up[]="Up";
const char gnome_stock_pixmap_down[]="Down";
const char gnome_stock_pixmap_top[]="Top";
const char gnome_stock_pixmap_bottom[]="Bottom";
const char gnome_stock_pixmap_attach[]="Attach";
const char gnome_stock_pixmap_index[]="Index";
const char gnome_stock_pixmap_font[]="Font";
const char gnome_stock_pixmap_exec[]="Exec";
const char gnome_stock_pixmap_align_left[]="Left";
const char gnome_stock_pixmap_align_right[]="Right";
const char gnome_stock_pixmap_align_center[]="Center";
const char gnome_stock_pixmap_align_justify[]="Justify";
const char gnome_stock_pixmap_text_bold[]="Bold";
const char gnome_stock_pixmap_text_italic[]="Italic";
const char gnome_stock_pixmap_text_underline[]="Underline";
const char gnome_stock_pixmap_text_strikeout[]="Strikeout";
const char gnome_stock_pixmap_text_indent[]="Text Indent";
const char gnome_stock_pixmap_text_unindent[]="Text Unindent";
const char gnome_stock_pixmap_colorselector[]="Color Selector";
const char gnome_stock_pixmap_add[]="Add";
const char gnome_stock_pixmap_remove[]="Remove";
const char gnome_stock_pixmap_table_borders[]="Table Borders";
const char gnome_stock_pixmap_table_fill[]="Table Fill";
const char gnome_stock_pixmap_text_bulleted_list[]="Text Bulleted List";
const char gnome_stock_pixmap_text_numbered_list[]="Text Numbered List";
const char gnome_stock_button_ok[]="Button_Ok";
const char gnome_stock_button_cancel[]="Button_Cancel";
const char gnome_stock_button_yes[]="Button_Yes";
const char gnome_stock_button_no[]="Button_No";
const char gnome_stock_button_close[]="Button_Close";
const char gnome_stock_button_apply[]="Button_Apply";
const char gnome_stock_button_help[]="Button_Help";
const char gnome_stock_button_next[]="Button_Next";
const char gnome_stock_button_prev[]="Button_Prev";
const char gnome_stock_button_up[]="Button_Up";
const char gnome_stock_button_down[]="Button_Down";
const char gnome_stock_button_font[]="Button_Font";
const char gnome_stock_menu_blank[]="Menu_";
const char gnome_stock_menu_new[]="Menu_New";
const char gnome_stock_menu_save[]="Menu_Save";
const char gnome_stock_menu_save_as[]="Menu_Save As";
const char gnome_stock_menu_revert[]="Menu_Revert";
const char gnome_stock_menu_open[]="Menu_Open";
const char gnome_stock_menu_close[]="Menu_Close";
const char gnome_stock_menu_quit[]="Menu_Quit";
const char gnome_stock_menu_cut[]="Menu_Cut";
const char gnome_stock_menu_copy[]="Menu_Copy";
const char gnome_stock_menu_paste[]="Menu_Paste";
const char gnome_stock_menu_prop[]="Menu_Properties";
const char gnome_stock_menu_pref[]="Menu_Preferences";
const char gnome_stock_menu_about[]="Menu_About";
const char gnome_stock_menu_scores[]="Menu_Scores";
const char gnome_stock_menu_undo[]="Menu_Undo";
const char gnome_stock_menu_redo[]="Menu_Redo";
const char gnome_stock_menu_print[]="Menu_Print";
const char gnome_stock_menu_search[]="Menu_Search";
const char gnome_stock_menu_srchrpl[]="Menu_Search/Replace";
const char gnome_stock_menu_back[]="Menu_Back";
const char gnome_stock_menu_forward[]="Menu_Forward";
const char gnome_stock_menu_first[]="Menu_First";
const char gnome_stock_menu_last[]="Menu_Last";
const char gnome_stock_menu_home[]="Menu_Home";
const char gnome_stock_menu_stop[]="Menu_Stop";
const char gnome_stock_menu_refresh[]="Menu_Refresh";
const char gnome_stock_menu_mail[]="Menu_Mail";
const char gnome_stock_menu_mail_rcv[]="Menu_Receive Mail";
const char gnome_stock_menu_mail_snd[]="Menu_Send Mail";
const char gnome_stock_menu_mail_rpl[]="Menu_Reply to Mail";
const char gnome_stock_menu_mail_fwd[]="Menu_Forward Mail";
const char gnome_stock_menu_mail_new[]="Menu_New Mail";
const char gnome_stock_menu_trash[]="Menu_Trash";
const char gnome_stock_menu_trash_full[]="Menu_Trash Full";
const char gnome_stock_menu_undelete[]="Menu_Undelete";
const char gnome_stock_menu_timer[]="Menu_Timer";
const char gnome_stock_menu_timer_stop[]="Menu_Timer Stop";
const char gnome_stock_menu_spellcheck[]="Menu_Spellchecker";
const char gnome_stock_menu_mic[]="Menu_Microphone";
const char gnome_stock_menu_line_in[]="Menu_Line In";
const char gnome_stock_menu_cdrom[]="Menu_Cdrom";
const char gnome_stock_menu_volume[]="Menu_Volume";
const char gnome_stock_menu_midi[]="Menu_Midi";
const char gnome_stock_menu_book_red[]="Menu_Book Red";
const char gnome_stock_menu_book_green[]="Menu_Book Green";
const char gnome_stock_menu_book_blue[]="Menu_Book Blue";
const char gnome_stock_menu_book_yellow[]="Menu_Book Yellow";
const char gnome_stock_menu_book_open[]="Menu_Book Open";
const char gnome_stock_menu_convert[]="Menu_Convert";
const char gnome_stock_menu_jump_to[]="Menu_Jump To";
const char gnome_stock_menu_up[]="Menu_Up";
const char gnome_stock_menu_down[]="Menu_Down";
const char gnome_stock_menu_top[]="Menu_Top";
const char gnome_stock_menu_bottom[]="Menu_Bottom";
const char gnome_stock_menu_attach[]="Menu_Attach";
const char gnome_stock_menu_index[]="Menu_Index";
const char gnome_stock_menu_font[]="Menu_Font";
const char gnome_stock_menu_exec[]="Menu_Exec";
const char gnome_stock_menu_align_left[]="Menu_Left";
const char gnome_stock_menu_align_right[]="Menu_Right";
const char gnome_stock_menu_align_center[]="Menu_Center";
const char gnome_stock_menu_align_justify[]="Menu_Justify";
const char gnome_stock_menu_text_bold[]="Menu_Bold";
const char gnome_stock_menu_text_italic[]="Menu_Italic";
const char gnome_stock_menu_text_underline[]="Menu_Underline";
const char gnome_stock_menu_text_strikeout[]="Menu_Strikeout";
