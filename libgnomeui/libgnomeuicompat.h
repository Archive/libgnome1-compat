/*
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
  @NOTATION@
 */

#ifndef LIBGNOMEUICOMPAT_H
#define LIBGNOMEUICOMPAT_H

#include <libgnomeui/gnome-app-util.h>
#include <libgnomeui/gnome-app-helper.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-appbar.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dock-band.h>
#include <libgnomeui/gnome-dock-item.h>
#include <libgnomeui/gnome-dock-layout.h>
#include <libgnomeui/gnome-dock.h>
#include <libgnomeui/gnome-messagebox.h>
#include <libgnomeui/gnome-pixmap.h>
#include <libgnomeui/gnome-preferences.h>
#include <libgnomeui/gnome-propertybox.h>
#include <libgnomeui/gnome-stock-ids.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-winhints.h>
#include <libgnomeui/gtkpixmapmenuitem.h>

#endif /* LIBGNOMEUICOMPAT_H */
